import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'package:flutter_doc/random_word.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Startup Name Generator',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.white,
          foregroundColor: Colors.pink,
        ),
      ),
      home: const RandomWords(),
    );
  }
}
